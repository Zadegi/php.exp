<?php
//    error_reporting(E_ALL);
//    ini_set('display_errors',1);

    include_once "config.php";
    $errorMsg = [];
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
	        $errorMsg[] = "ایمیل وارد شده معتبر نمی باشد.";
        }
        checkPassword($_POST['password1'],$errorMsg);
        if($_POST['password1'] != $_POST['password2']){
	        $errorMsg[] = "رمز عبور و تکرار آن باید یکسان باشند";
        }


        if(empty($errorMsg)){
	        $res = register($_POST['email'],$_POST['password1'],$_POST['displayName']);
	        if($res == -1 ){
		        $errorMsg[] = "یک کاربر با این ایمیل قبلا در سایت ثبت نام کرده است.";
	        }
        }

    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ثبت نام</title>
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/milligram-rtl.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="continer">

    <?php if($_SERVER["REQUEST_METHOD"] == "POST"):  ?>
    <?php if(empty($errorMsg)):  ?>
    <div class="centerBox success">
        ثبت نام شما با موفقیت انجام شد.
    </div>
    <?php else:   ?>
    <div class="centerBox error">
    <?php
	    foreach ( $errorMsg as $eMsg ) {
            echo "$eMsg<br>";
        }
        ?>
    </div>
    <?php endif;   ?>
    <?php endif;   ?>
    <div class="centerBox">
        <h3>ثبت نام در سایت</h3>
        <form action="register.php" method="post">
            <input type="text" name="displayName" placeholder="نام کامل شما" value="<?php echo $_POST['displayName'] ?? ""; ?>">
            <input type="text" class="ltr text-left" name="email" placeholder="ایمیل"value="<?php echo $_POST['email'] ?? ""; ?>">
            <input type="password" class="ltr text-left" name="password1" placeholder="رمز عبور">
            <input type="password" class="ltr text-left" name="password2" placeholder="تکرار رمز عبور">
            <input type="submit" value="ثبت نام">
        </form>
    </div>
</div>

</body>
</html>