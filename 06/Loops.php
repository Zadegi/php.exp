<?php
include "../common/common.php";
$i = 1;
while (true){
	myPrint("Salam $i");
	$i++;
	if($i > 5)
		break;
}

myPrint("-----------do..while-----------");
$i = 1;
do {
	myPrint("Salam $i");
	$i++;
	if($i > 5)
		break;
}while(true);
myPrint("-----------for-----------");
for($i = 19; $i>=0 ;$i-=2){
	myPrint("Salam $i");
}
myPrint("-----------foreach-----------");
$arr = [1,2,'c' => 3,4,[5.1,[5.21,[5.211,5.216]],5.7],6,7];
/*for ($i = 0 ; $i<7 ; $i++){
	myPrint($arr[$i]);
}*/
printArr($arr);
